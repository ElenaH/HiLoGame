package com.playtech.common;

import java.util.*;

public class ShuffledDeck {
	List <Card> deck = new ArrayList<Card> ();
			
	public ShuffledDeck(){		
		reshuffle();
	}
	
	public Card getAndDiscardTop(){
		Card card = deck.remove(0);
		if (deck.isEmpty()){
			reshuffle();	}
		return card;
	}
	
	private void reshuffle(){
		this.deck=getShuffledDeck();
	}
	
	private static List <Card> getShuffledDeck(){
		List <Card> deck = new ArrayList<Card> ();
		for (Card card: Card.values()){
			deck.add(card);
		}		
		Collections.shuffle(deck);	
		return deck;
	}
}