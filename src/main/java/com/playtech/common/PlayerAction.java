package com.playtech.common;

public enum PlayerAction {
	HIGHER, LOWER, EQUALS, ILLEGAL_ACTION;

	public static PlayerAction fromString(String string) {
		switch (string.trim().toUpperCase()) {
		case "HIGHER":
			return HIGHER;
		case "LOWER":
			return LOWER;
		case "EQUALS":
			return EQUALS;
		default:
			return ILLEGAL_ACTION;
		}
	}

	public static PlayerAction fromChar(char ch) {
		switch (ch) {
		case 'H':
			return HIGHER;
		case 'h':
			return HIGHER;
		case 'L':
			return LOWER;
		case 'l':
			return LOWER;
		case 'E':
			return EQUALS;
		case 'e':
			return EQUALS;
		default:
			return ILLEGAL_ACTION;
		}
	}
}