package com.playtech.server.impl;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.playtech.common.*;
import com.playtech.game.protocol.*;
import com.playtech.server.api.*;

public class GameServiceImpl  implements GameService, Runnable {	
	private final static Logger SERVER_LOGGER = Logger.getLogger(GameServiceImpl.class);
	private boolean keepProcessing = true;
	private final ShuffledDeck myDeck;
	private Socket socket;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private long roundId;
	private Card baseCard;
	
	public GameServiceImpl(Socket socket) {
		this.socket = socket;
		this.myDeck= new ShuffledDeck();
		this.baseCard = myDeck.getAndDiscardTop();
		this.roundId = 0;
	}

	public static void main(String[] args) {
		final int SERVER_SOCKET_PORT = 1337;
		final int THREAD_POOL_LIMIT = 4;
		ExecutorService poolForGamingServers = Executors.newFixedThreadPool(THREAD_POOL_LIMIT);
		SERVER_LOGGER.info("Server is running.");
		try (ServerSocket listener = new ServerSocket(SERVER_SOCKET_PORT)) {
			while (true) {
				poolForGamingServers.execute(new GameServiceImpl(listener.accept()));
			}
		} catch (Exception e) {
			SERVER_LOGGER.error("Server runtime errors: " + e);
		}
	}
	
	@Override
	public void run() {		
		SERVER_LOGGER.info(this + ": New Server Thread started and listening to the port " + socket.getPort());
		PlayerActionRequest actionRequest;
		PlayerAction action;
		try {
           openResources();
			while (keepProcessing) {
				roundId++;
				try {
					sendStartRoundRequest();
					actionRequest = getPlayerActionRequest();
					action = actionRequest.getPlayerAction();
					sendPlayerActionResponse(playerAction(actionRequest));
					baseCard = sendFinishRoundRequestAndGetNewCard(action);					
				} catch (Exception e) {
					SERVER_LOGGER.error("Message exchange error: " + e);
					stopProcessingAndCloseIgnoringException();					
				}				
			}
		} catch (Exception e) {
			SERVER_LOGGER.info("Socket connection error: ", e);
		}
	}

	private void openResources() throws Exception {
		SERVER_LOGGER.info(this + ": is using socket port: " + socket.getPort());
		output = new ObjectOutputStream(socket.getOutputStream());
		input = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
	}
	
	protected void sendStartRoundRequest() throws Exception {
		final int ROUND_DURATION = 10000;
		long currentTimeStamp = new Timestamp(System.currentTimeMillis()).getTime();
		StartRoundRequest startRequest = new StartRoundRequest(ROUND_DURATION, currentTimeStamp, roundId, baseCard);
		output.writeObject(startRequest);
		output.flush();
	}

	protected PlayerActionRequest getPlayerActionRequest() throws Exception {
		return (PlayerActionRequest) input.readObject();
	}	
	
	protected void sendPlayerActionResponse
	(PlayerActionResponse playerActionResponse) throws Exception {
		output.writeObject(playerActionResponse);
		output.flush();		
	}

	public PlayerActionResponse playerAction(PlayerActionRequest playerActionRequest) {
		PlayerAction action = playerActionRequest.getPlayerAction();
		if (action == null) {
			return new PlayerActionResponse("Action was not created.");
		} else if (PlayerAction.ILLEGAL_ACTION.equals(action)) {
			return new PlayerActionResponse("Action created empty, user did not provide valid input.");
		} else {
			return new PlayerActionResponse("");
		}
	}

	protected Card sendFinishRoundRequestAndGetNewCard(PlayerAction action) throws Exception {
		Card newCard = myDeck.getAndDiscardTop();
		boolean win = win(newCard, action);
		FinishRoundRequest finishRequest = new FinishRoundRequest(roundId, win);
		output.writeObject(finishRequest);
		output.flush();
		return newCard;		
	}
	
	@SuppressWarnings("incomplete-switch")
	protected boolean win(Card newCard, PlayerAction action) {
		boolean win = false;
		int comparisonResult = newCard.getValue().compareTo(baseCard.getValue());
		switch (action) {
		case HIGHER:
			if (comparisonResult > 0) {
				win = true;
			}
			break;
		case LOWER:
			if (comparisonResult < 0) {
				win = true;
			}
			break;
		case EQUALS:
			if (comparisonResult == 0) {
				win = true;
			}
			break;
		}
		return win;
	}
	
	public void stopProcessingAndCloseIgnoringException() {
		keepProcessing = false;
		try {
			socket.close();
		} catch (IOException ignore) {
		}
	}
}