package com.playtech.client.impl;

import java.io.*;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.*;

import org.apache.log4j.Logger;

import com.playtech.client.api.GameClient;
import com.playtech.common.*;
import com.playtech.game.protocol.*;

public class GameClientImpl implements GameClient {
	private final static Logger CLIENT_LOGGER = Logger.getLogger(GameClientImpl.class);
	private boolean keepProcessing = true;
	private BufferedReader consoleReader;
	private Socket socket;
	private ObjectOutputStream output;
	private ObjectInputStream input;

	public static void main(String[] args) {
		new GameClientImpl().run();	
	}
	
	public void run() {
		CLIENT_LOGGER.info(this + ": Client started.");
		int serverPort = 1337;
		try {
			openResources(serverPort);
			while (keepProcessing) {
				startRound(getStartRoundRequest());
				finishRound(getFinishRoundRequest());
			}
		} catch (Exception e) {
			CLIENT_LOGGER.error("Socket connection error: " + e);
		} finally {
			stopProcessingAndCloseIgnoringException();
		}
	}

	protected void openResources(int serverPort) throws Exception {
		socket = new Socket("localhost", serverPort);
		CLIENT_LOGGER.info(this + ": is using socket port: " + socket.getPort());
		consoleReader = new BufferedReader(new InputStreamReader(System.in));
		output = new ObjectOutputStream(socket.getOutputStream());
		input = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
	}

	@Override
	public void startRound(StartRoundRequest startRoundRequest) {
		try {
			displayStartRoundInfo(startRoundRequest);
			sendActionRequest(waitAndGenerateAction(startRoundRequest));
			displayErrorText(getPlayerActionResponse());
		} catch (Exception e) {
			CLIENT_LOGGER.error("Start round error: " + e);
		}
	}

	protected StartRoundRequest getStartRoundRequest() throws Exception {
		return (StartRoundRequest) input.readObject();
	}

	protected void displayStartRoundInfo(StartRoundRequest startRoundRequest) {
		CLIENT_LOGGER.info("Round " + startRoundRequest.getRoundId() + " has started.");
		CLIENT_LOGGER.info("***Base card = " + startRoundRequest.getBaseCard() + ".***");
		CLIENT_LOGGER.info("Please, press h (higher), l (lower) or e (equals). Then press enter.");
	}

	protected void sendActionRequest(PlayerAction action) throws Exception {
		PlayerActionRequest actionRequest = new PlayerActionRequest(action);
		output.writeObject(actionRequest);
		output.flush();
	}

	protected PlayerAction waitAndGenerateAction(StartRoundRequest startRoundRequest) throws Exception {
		showCountdown(startRoundRequest);
		System.out.println();
		return getInput();
	}

	protected void showCountdown(StartRoundRequest startRoundRequest) throws Exception {
		int duration = startRoundRequest.getActionRoundDuration();
		long startTime = startRoundRequest.getActionRoundStartTimestamp();
		TimerTask countDown = new CountDown(duration / 1000);
		Timer timer = new Timer();
		System.out.print("Time left to bet : ");
		timer.schedule(countDown, new Date(startTime), 1000L);
		do {
		} while (!(consoleReader.ready() || isTimeUp(startTime + duration)));
		timer.cancel();
	}

	protected PlayerAction getInput() throws Exception {
		String line;
		char symbol = ' ';
		 if (!consoleReader.ready()) {
		 } else if ((line = consoleReader.readLine()).isEmpty()) {
		 } else {
			symbol = line.charAt(0);
		 }
		CLIENT_LOGGER.info("User input was : " + symbol);
		return PlayerAction.fromChar(symbol);
	}

	protected boolean isTimeUp(long finishTime) {
		long currentTime = new Timestamp(System.currentTimeMillis()).getTime();
		return (finishTime < currentTime);
	}

	protected PlayerActionResponse getPlayerActionResponse() throws Exception {
		return (PlayerActionResponse) input.readObject();
	}

	protected void displayErrorText(PlayerActionResponse playerActionResponse) {
		String error = playerActionResponse.getErrorText();
		if (error.isEmpty()) {
			CLIENT_LOGGER.info("Player action sent successfully.");
		} else {
			CLIENT_LOGGER.error("Errors occured while sending player message: " + error);
		}
	}

	@Override
	public void finishRound(FinishRoundRequest finishRoundRequest) {
		try {
			displayFinishRoundInfo(finishRoundRequest);
		} catch (Exception e) {
			CLIENT_LOGGER.error("Finish round error: " + e);
		}
	}

	protected FinishRoundRequest getFinishRoundRequest() throws Exception {
		return (FinishRoundRequest) input.readObject();
	}

	protected void displayFinishRoundInfo(FinishRoundRequest finishRoundRequest) {
		CLIENT_LOGGER.info("Round " + finishRoundRequest.getRoundId() + " has finished.");
		if (finishRoundRequest.isWin()) {
			CLIENT_LOGGER.info("***You won! Try again!***");
		} else {
			CLIENT_LOGGER.info("***Sorry, you lost this round. Try again!***");
		}
	}

	public void stopProcessingAndCloseIgnoringException() {
		keepProcessing = false;
		try {
			consoleReader.close();
			socket.close();
		} catch (IOException ignore) {
		}
	}
}

class CountDown extends TimerTask {
	private int seconds;

	CountDown(int seconds) {
		this.seconds = seconds;
	}

	@Override
	public void run() {
		System.out.print((seconds--) + "...");
	}
}